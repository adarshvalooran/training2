<?php
require 'fun.php';
if (isset($_POST['submit'])) {
  $title = $_POST['title'];
  $blog = $_POST['content'];
  $tag = $_POST['tags'];
}
$date = date("Y-m-d H:i:s");
$words = trimInput($tag);
$s = sizeof($words);
$conn = db_get_connection(); 
insertBlog($title, $blog, $date, $conn);
for ($i = 0; $i < $s; $i++) {
  $count = tagCount($conn, $words, $i);
  if ($count == 0) {
    insertTag($conn, $words, $i);
  }  
  insertRel($conn, $words, $title, $i);
}
header("Location:index.php");
?>