<?php  
require 'fun.php';
$conn = db_get_connection();
$tagIdValue = $_GET['tag'];
if (isset($_GET['pageno'])) {
  $pageno = $_GET['pageno'];
} else {
  $pageno = 1;
}
$n = 3;
$total_pages = pageTag($conn, $n, $tagIdValue);
if ($pageno > $total_pages) {
  $pageno = 1;
}
$offset = ($pageno-1) * $n;
$sort = $_GET['sort'];
$d = sortDispTag($conn, $offset, $sort, $n, $tagIdValue);
?>


<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Test Blog</title>
  <!-- Bootstrap core CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template -->
  <link href="css/all.min.css" rel="stylesheet" type="text/css">
  <link href='https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
  <!-- Custom styles for this template -->
  <link href="css/clean-blog.min.css" rel="stylesheet">
</head>

<body>
  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    <div class="container">
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link" href="index.php">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="add.php">Add Blog</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
  <!-- Page Header -->
  <header class="masthead" style="background-image: url('img/home-bg.jpg')">
    <div class="overlay"></div>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <div class="site-heading">
            <h1>Tag Posts</h1>
          </div>
        </div>
      </div>
    </div>
  </header>
  <!-- Main Content -->
  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-md-10 mx-auto">              
        <div class="dropdown">
          <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Sort by
          <span class="caret"></span></button>
          <ul class="dropdown-menu">  
            <li><a href="tagpost.php?sort=ASC&pageno=<?php echo $pageno; ?>&tag=<?php echo $_GET['tag']; ?>">Ascending</a></li>
            <li><a href="tagpost.php?sort=DESC&pageno=<?php echo $pageno; ?>&tag=<?php echo $_GET['tag']; ?>">Descending</a></li>
          </ul>
        </div>
        <?php
        if (isset($d)) {
          foreach ($d as $row) {
            $val = $row["id"];
            $cont = trimContent($row);
            echo '<div class="post-preview"> 
              <a href="post.php?id='.$val.'">
              <h2 class="post-title">'.$row["Title"].'</h2>
              <h3 class="post-subtitle">'.$cont.'</h3>
              </a><p class="post-meta">Posted on '.$row["Date"].'</p>  ';
            $data2 = relatedTags($conn, $val);
            echo "<p>Tags: ";
            if (isset($data2)) { 
              foreach ($data2 as $row2) {
              $tagidval = $row2["tid"];
              echo '<a href="tagpost.php?sort='.$sort.'&pageno='.$pageno.'&tag='.$tagidval.'">'.$row2["tname"].' </a>';      
              }
            }
            echo "</p>
            </div>
            <hr>";
          }  
        } else {
          echo "0 results";
        }
        ?>

        <div class="clearfix">
           <ul class="pagination">  
            <li  class="<?php if ($pageno == 1) { echo 'disabled'; } ?>">
              <a class="btn btn-primary float-right" href="<?php if ($pageno == 1) { echo '#'; } else { echo "tagpost.php?sort=$sort&tag=$tagIdValue&pageno=".($pageno - 1); } ?>">Prev  </a>
            </li>
            <li  class="<?php if ($pageno == $total_pages) { echo 'disabled'; } ?>">
              <a class="btn btn-primary float-right ralign" href="<?php if ($pageno == $total_pages) { echo '#'; } else { echo "tagpost.php?sort=$sort&tag=$tagIdValue&pageno=".($pageno + 1); } ?>">  Next</a>
            </li>  
          </ul>
        </div>  
  <!-- Bootstrap core JavaScript -->
  <script src="js/jquery.min.js"></script>
  <script src="js/bootstrap.bundle.min.js"></script>
  <!-- Custom scripts for this template -->
  <script src="js/clean-blog.min.js"></script>
</body>

</html>