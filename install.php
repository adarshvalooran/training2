<!DOCTYPE html>

<script>
function check(ch) {
  var checkBox = document.getElementById("dummyno");
  checkBox.disabled = ch.checked ? false : true;
  if (checkBox.disabled ) {
    checkBox.style.display = "none";
  }
  else {
    checkBox.style.display = "block";
  }
}
</script>

<?php
require 'fun.php';
if (isset($_POST['submit'])) {
  $db = $_POST['db'];
  $username = $_POST['username'];
  $password = $_POST['password'];
  if ($username == "root" and $password == "user0123") {
    createFile($username, $password, $db);
    require 'config.php';
    try {
      $conn = new PDO("mysql:host=$hostname", $username, $password);
      $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      dropDb($conn, $dbname);
      createDb($conn, $dbname);
      createTable($conn);
      $dummyNo = $_POST['dummyNo']; 
      while ($dummyNo != 0)  {
        insertDummy($conn);
        $dummyNo--;
      }
    }
    catch(PDOException $e) {
      $conn->rollback();
      echo "Error: " . $e->getMessage();
    }
    header("Location:index.php");
  } else {
    echo "<script type='text/javascript'> alert('Username or Password is incorrect');</script>";
  }
}   
?>

<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Test Blog</title>
  <!-- Bootstrap core CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template -->
  <link href="css/all.min.css" rel="stylesheet" type="text/css">
  <link href='https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
  <!-- Custom styles for this template -->
  <link href="css/clean-blog.min.css" rel="stylesheet">
</head>

<body>
  <!-- Page Header -->
  <header class="masthead" style="background-image: url('img/home-bg.jpg')">
    <div class="overlay"> </div>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <div class="site-heading">
            <h1>Configuration</h1>
          </div>
        </div>
      </div>
    </div>
  </header>
<!-- Main Content -->
<div class="container">
  <div class="row">
    <div class="col-lg-8 col-md-10 mx-auto">  
      <form name="blogform" action="install.php" method="POST">
        <div class="control-group">
          <div class="form-group floating-label-form-group controls">
            <label>Username</label>
            <input type="text" class="form-control" placeholder="Enter Username" name="username" required data-validation-required-message="Enter valid username ">
            <p class="help-block text-danger"></p>
          </div>
        </div>
         <div class="control-group">
          <div class="form-group floating-label-form-group controls">
            <label>Password</label>
            <input type="password" class="form-control" placeholder="Password" name="password" required data-validation-required-message="Enter a password.">
            <p class="help-block text-danger"></p>
          </div>
        </div>
        <div class="control-group">
          <div class="form-group floating-label-form-group controls">
            <label>Database</label>
            <input type="text" class="form-control" placeholder="Enter Database Name" name="db" required data-validation-required-message="Enter another"></textarea>
            <p class="help-block text-danger"></p>
          </div>
        </div>
            Create Dummy Content<input type="checkbox"  id="myCheck" onclick="check(this)">
            <input id="dummyno" type="number" disabled="disabled" class="form-control" name="dummyNo">
            <br>
          <div id="success"></div>
        <div class="form-group">
        <button type="submit" class="btn btn-primary" name= "submit">Go</button>
          </div>
        </div>
      </form>
    </div>            
  </div>                  
  <!-- Bootstrap core JavaScript -->
  <script src="js/jquery.min.js"></script>
  <script src="js/bootstrap.bundle.min.js"></script>
  <!-- Custom scripts for this template -->
  <script src="js/clean-blog.min.js"></script>
</body>

</html>
